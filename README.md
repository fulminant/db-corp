# DevBranch comporate site.

#### Used technologies:
* ES6
* Babel
* Gulp
* SASS
* ESlint

### Installation

db-corp requires [Node.js](https://nodejs.org/) v6+ to run.

Clone repo
```sh
$ git clone git@bitbucket.org:fulminant/db-corp.git
```

Install the dependencies and start the server.

```sh
$ cd db-corp
$ npm install
$ gulp
```

After start you could open in your browser localhost:3000/#/ and check tooltips implementation.

#### Gulp tasks
* ```gulp compile``` - for compile scripts and styles.
* ```gulp compile:styles``` - for compile styles.
* ```gulp compile:scripts``` - for compile scripts.
* ```gulp watch``` - for start watching project directory to scripts or styles changes.
* ```gulp scripts:lint``` - for check scripts quality and style.
* ```gulp serve``` - for start project to browser.