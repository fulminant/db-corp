'use strict';

const gulp = require('gulp');
const bs = require('browser-sync').create();

// Project path's with js and styles.
const paths = {
	styles: `${__dirname}/src/styles.scss`,
	stylesWatch: `${__dirname}/src/**/*.scss`,
	dist: `${__dirname}/dist`,
	serveDirs: [`${__dirname}/dist`, __dirname],
	scripts: `${__dirname}/src/js/**/*.js`,
	partialWatch: `${__dirname}/**/*.html`
};

// Browser sync settings.
const config = {
	startBrowser: false,
	autoReload: true,
	host: 'localhost',
	port: 3333
};

// Import tasks.
const serve = require('./tasks/server')(bs, paths.serveDirs, config);
const styles = require('./tasks/styles')(gulp, paths.styles, paths.dist, bs, config.autoReload);
const scripts = require('./tasks/scripts')(gulp, paths.scripts, paths.dist, bs, config.autoReload);
const eslint = require('./tasks/eslint')(gulp, paths.scripts);

// Task for lint js.
gulp.task('scripts:lint', [], eslint);

// Serve task.
gulp.task('serve', ['compile'], serve);

// Compile tasks.
gulp.task('compile', ['compile:styles', 'compile:scripts']);
gulp.task('compile:styles', [], styles);
gulp.task('compile:scripts', ['scripts:lint'], scripts);

// Watch tasks.
gulp.task('watch', ['compile'], () => {
	gulp.watch(paths.stylesWatch, ['compile:styles'], () => {
		console.log(paths.stylesWatch, 'dd');
	});
	gulp.watch(paths.scripts, ['compile:scripts']);
	gulp.watch(paths.partialWatch).on('change', bs.reload);
});

// Default gulp task.
gulp.task('default', ['serve', 'watch']);