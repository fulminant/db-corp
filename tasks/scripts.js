module.exports = (gulp, scripts, dist, bs, autoReload) => {
	return () => {
		const babel = require('gulp-babel');
		const sourceMaps = require('gulp-sourcemaps');
		const plumber = require('gulp-plumber');
		const gulpIf = require('gulp-if');
		const notify = require('gulp-notify');
		const concat = require('gulp-concat');

		return gulp.src(scripts)
			.pipe(sourceMaps.init())
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(babel({
				presets: ['env']
			}))
			.pipe(concat('main.js'))
			.pipe(sourceMaps.write('.'))
			.pipe(gulp.dest(dist))
			.pipe(gulpIf(autoReload, bs.reload({stream:true})));
	};
};