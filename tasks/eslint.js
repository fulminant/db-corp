module.exports = (gulp, scripts) => {
	return () => {
		const eslint = require('gulp-eslint');
		const plumber = require('gulp-plumber');
		const notify = require('gulp-notify');

		return gulp.src(scripts)
			.pipe(eslint())
			.pipe(plumber())
			.pipe(eslint.format())
			.pipe(eslint.failAfterError())
			.on("error", notify.onError("Error: <%= error.message %>"));

	};
};