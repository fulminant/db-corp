module.exports = (gulp, styles, dist, bs, autoReload) => {
	return () => {
		const sass = require('gulp-sass');
		const sourceMaps = require('gulp-sourcemaps');
		const plumber = require('gulp-plumber');
		const autoPrefixer = require('gulp-autoprefixer');
		const gulpIf = require('gulp-if');
		const notify = require('gulp-notify');

		return gulp.src(styles)
			.pipe(sourceMaps.init())
			.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
			.pipe(sass())
			.pipe(autoPrefixer({
				browsers: ['> 0%'],
				cascade: false
			}))
			.pipe(sourceMaps.write())
			.pipe(gulp.dest(dist))
			.pipe(gulpIf(autoReload, bs.reload({stream:true})));
	};
};