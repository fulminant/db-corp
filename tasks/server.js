module.exports = (bs ,serverRootDirs, configs) => {
	return () => {
		const onOffFlag = '✗';
		const browserSync = bs.init({
			server: {
				baseDir: serverRootDirs
			},
			open: configs.startBrowser,
			host: configs.host,
			browser: 'default',
			notify: false,
			port: configs.port
		});

		console.log(`Server started ${onOffFlag}\n on port ${configs.port}`);

		return browserSync;
	};
};