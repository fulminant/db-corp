jQuery(document).ready(($) => {
	$('#fullPageBlock').fullpage({
		anchors: [
			'first',
			'second',
			'third',
			'fourth',
			'fifth',
			'sixth',
			'seventh',
			'eight',
			'ninth',
			'tenth'
		]
	});
});